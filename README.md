# Strings Good

Strings Good is a C library providing strings that are better than `char *` but still simple. It implements the basic ideas in "[How To Strings Good](https://twitter.com/alexbuzzbee/status/1011706869763788800)."

Strings Good provides a structure called `SGString` and a number of functions that operate on `SGString`s. `SGString`s are encoded in UTF-8; functions are available to encode from and decode to Unicode codepoints.

Unfortunately, because UTF-8 uses the high bit, Strings Good requires `char` to be unsigned. See your compiler's documentation; for `gcc` and `clang`, use option `-funsigned-char`.