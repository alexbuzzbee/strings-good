#include "check_sgstring.h"
#include "../sgstring.h"

START_TEST (baseTest) {
  struct SGString *str = sgNew("abcd");

  ck_assert_str_eq(str->chars, "abcd");
  ck_assert_int_eq(str->len, 4);

  sgDestroy(str);
} END_TEST

START_TEST (boundsTest) {
  struct SGString *str = sgNew("abcd");

  ck_assert_msg(sgCheckBounds(str, 3), "Bounds check thinks end of string is too early.");
  ck_assert_msg(!sgCheckBounds(str, 4), "Bounds check thinks end of string is too late.");

  sgDestroy(str);
} END_TEST

START_TEST (copyTest) {
  struct SGString *str = sgNew("abcd");
  struct SGString *newStr = sgCopy(str);

  ck_assert(newStr != str);
  ck_assert(newStr->chars != str->chars);
  ck_assert(newStr->len == str->len);
  ck_assert_str_eq(str->chars, newStr->chars);

  sgDestroy(str);
  sgDestroy(newStr);
} END_TEST

START_TEST (substrTest) {
  struct SGString *str = sgNew("abcd");
  struct SGString *subStr = sgSubstring(str, 1, 2);

  ck_assert_str_eq(subStr->chars, "bc");

  sgDestroy(str);
  sgDestroy(subStr);
} END_TEST

START_TEST (concatTest) {
  struct SGString *str1 = sgNew("ab");
  struct SGString *str2 = sgNew("cd");
  struct SGString *catStr = sgConcat(str1, str2);

  ck_assert_str_eq(catStr->chars, "abcd");

  sgDestroy(str1);
  sgDestroy(str2);
  sgDestroy(catStr);
} END_TEST

Suite *sgStringSuite() {
  Suite *suite = suite_create("sgstring");
  TCase *base = tcase_create("base");
  TCase *bounds = tcase_create("bounds");
  TCase *copy = tcase_create("copy");
  TCase *substr = tcase_create("substr");
  TCase *concat = tcase_create("concat");

  tcase_add_test(base, &baseTest);
  suite_add_tcase(suite, base);
  tcase_add_test(bounds, &boundsTest);
  suite_add_tcase(suite, bounds);
  tcase_add_test(copy, &copyTest);
  suite_add_tcase(suite, copy);
  tcase_add_test(substr, &substrTest);
  suite_add_tcase(suite, substr);
  tcase_add_test(concat, &concatTest);
  suite_add_tcase(suite, concat);

  return suite;
}