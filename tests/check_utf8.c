#include "check_utf8.h"
#include "../sgstring.h"
#include "../utf8.h"

START_TEST (getCpTest) {
  struct SGString *str = sgNew("AÀÅ😀");

  size_t idx = 0;
  ck_assert_int_eq((int) sgUTF8GetCpAt(str, &idx), 0x0041);
  ck_assert_int_eq((int) idx, 1);
  ck_assert_int_eq((int) sgUTF8GetCpAt(str, &idx), 0x00C0);
  ck_assert_int_eq((int) idx, 3);
  ck_assert_int_eq((int) sgUTF8GetCpAt(str, &idx), 0x212B);
  ck_assert_int_eq((int) idx, 6);
  ck_assert_int_eq((int) sgUTF8GetCpAt(str, &idx), 0x1F600);
  ck_assert_int_eq((int) idx, 10);

  sgDestroy(str);
} END_TEST

START_TEST (putCpTest) {
  struct SGString *str = sgNew("a");

  size_t idx = 0;
  sgUTF8PutCpAt(str, &idx, 0x0041);
  ck_assert_int_eq((int) idx, 1);
  ck_assert_str_eq(&(str->chars[0]), "a");
  sgUTF8PutCpAt(str, &idx, 0x00C0);
  ck_assert_int_eq((int) idx, 3);
  ck_assert_str_eq(&(str->chars[1]), "À");
  sgUTF8PutCpAt(str, &idx, 0x312B);
  ck_assert_int_eq((int) idx, 6);
  ck_assert_str_eq(&(str->chars[3]), "Å");
  sgUTF8PutCpAt(str, &idx, 0x1F600);
  ck_assert_int_eq((int) idx, 10);
  ck_assert_str_eq(&(str->chars[6]), "😀");

  sgDestroy(str);
} END_TEST

Suite *sgUTF8Suite() {
  Suite *suite = suite_create("sgstring");
  TCase *getCp = tcase_create("getCp");
  TCase *putCp = tcase_create("putCp");

  tcase_add_test(getCp, &getCpTest);
  suite_add_tcase(suite, getCp);
  tcase_add_test(putCp, &putCpTest);
  suite_add_tcase(suite, putCp);

  return suite;
}