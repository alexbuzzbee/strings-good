#include "check_sgstring.h"
#include "check_utf8.h"

int main() {
  int failed = 0;
  Suite *strSuite = sgStringSuite();
  SRunner *strRun = srunner_create(strSuite);
  Suite *utf8Suite = sgUTF8Suite();
  SRunner *utf8Run = srunner_create(utf8Suite);

  srunner_run_all(strRun, CK_NORMAL);
  failed = srunner_ntests_failed(strRun);
  srunner_free(strRun);
  srunner_run_all(utf8Run, CK_NORMAL);
  failed += srunner_ntests_failed(utf8Run);
  srunner_free(utf8Run);
  return failed == 0 ? 0 : 1;
}