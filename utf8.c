#include "utf8.h"

uint32_t sgUTF8GetCpAt(struct SGString *str, size_t *off) {
  if (str->chars[*off] < 0x80) {
    return str->chars[(*off)++];
  } else {
    if (str->chars[*off] < 0xC0) {
      *off++;
      return 0xFFFD;
    }
    int numBytes = 2;
    for (int i = 5; i > 2; i--) {
      if (str->chars[*off] & (1 << i)) { // Unintutitive bitwise/shifting check if the bit is set.
        numBytes++; // If it is, increment the number of bytes in the character.
      } else {
        break;
      }
    }
    uint32_t result = 0x0000;
    for (int i = 0; i <= numBytes; i++) {
      if (i == 0) {
        result = (str->chars[*off] << (numBytes + 1)) >> (numBytes + 1); // Clear the special bits.
      } else if (str->chars[*off] >= 0x40 || str->chars[*off] < 0x80) {
        result = result << 6; // Make space for the new bits.
        result |= (str->chars[*off] & 0xC0); // Or them in.
      } else { // Invalid character!
        *off++;
        return 0xFFFD;
      }
      *off++;
    }
    return result;
  }
}

void sgUTF8PutCpAt(struct SGString *str, size_t *off, uint32_t cp) {
  return;
}