#include "sgstring.h"
#include <string.h>
#include <stdlib.h>

struct SGString *sgNew(const char *chars) {
  struct SGString *str = malloc(sizeof(struct SGString));
  str->len = strlen(chars);
  char *buf = malloc(str->len);
  strcpy(buf, chars);
  str->chars = buf;
  return str;
}

void sgDestroy(struct SGString *str) {
  free((void *) str->chars);
  free((void *) str);
}

bool sgCheckBounds(struct SGString *str, size_t off) { // Use size_t instead of off_t for portability.
  return off < str->len;
}

struct SGString *sgCopy(struct SGString *str) {
  return sgNew(str->chars);
}

struct SGString *sgSubstring(struct SGString *str, size_t off, size_t len) {
  if (!sgCheckBounds(str, off)) {
    return NULL;
  } else if (!sgCheckBounds(str, off + len)) {
    len = str->len - off;
  }
  char *buf = malloc(len + 1);
  strncpy(buf, &(str->chars[off]), len);
  return sgNew(buf);
}

struct SGString *sgConcat(struct SGString *str1, struct SGString *str2) {
  struct SGString *newStr = sgNew(str1->chars);
  newStr->len = str1->len + str2->len;
  newStr->chars = realloc(newStr->chars, newStr->len + 1);
  strcat(newStr->chars, str2->chars);
  return newStr;
}
