.PHONY: all clean

all: libstringsgood.a sgtest

test: sgtest
	./sgtest

clean:
	- rm sgstring.o
	- rm libstringsgood.a
	- rm sgtest

sgtest: libstringsgood.a tests/sgtest.c tests/check_sgstring.c tests/check_sgstring.h
	cc tests/sgtest.c tests/check_sgstring.c tests/check_utf8.c -L. -lstringsgood -lcheck -o sgtest -funsigned-char

libstringsgood.a: sgstring.o utf8.o
	ar -r libstringsgood.a sgstring.o utf8.o

string.o: sgstring.c sgstring.h
	cc -c sgstring.c -o sgstring.o -funsigned-char

utf8.o: utf8.c utf8.h sgstring.h
	cc -c utf8.c -o utf8.o -funsigned-char
