#include <stddef.h>
#include <stdbool.h>

#pragma once

/**
 * A bounded string.
 * 
 * Based on char * with the addition of a stored length and some related functions to manipulate it.
 */
struct SGString {
  char *chars;
  size_t len;
};

/**
 * Creates a new SGString from the specified char * or const char *.
 * 
 * @param chars The characters of the string.
 * 
 * @return A new SGString containing a copy of chars.
 */
struct SGString *sgNew(const char *chars);

/**
 * Destroys an existing SGString and releases its memory.
 * 
 * @param str The string to destroy.
 */ 
void sgDestroy(struct SGString *str);

/**
 * Checks that the specified offset is inside the string.
 * 
 * @param str The string on which to check bounds.
 * @param off The offset to check.
 * 
 * @return Whether off falls within str.
 */
bool sgCheckBounds(struct SGString *str, size_t off);

/**
 * Copies an SGString.
 * 
 * @param str The string to copy.
 * 
 * @return A new SGString copied from str.
 */
struct SGString *sgCopy(struct SGString *str);

/**
 * Gets a substring.
 * 
 * @param str The string to get a subset of.
 * @param off The starting offset of the substring.
 * @param len The length of the substring.
 * 
 * @return A new SGString containing the substring.
 */
struct SGString *sgSubstring(struct SGString *str, size_t off, size_t len);

/**
 * Concatenates two SGStrings and returns the result.
 * 
 * Does not clobber either of the input strings.
 * 
 * @param str1 The first string.
 * @param str2 The second string.
 * 
 * @return A new SGString, the result of concatenating the arguments.
 */
struct SGString *sgConcat(struct SGString *str1, struct SGString *str2);
