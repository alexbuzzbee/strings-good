#include <stdint.h>
#include "sgstring.h"

#pragma once

uint32_t sgUTF8GetCpAt(struct SGString *str, size_t *off);
void sgUTF8PutCpAt(struct SGString *str, size_t *off, uint32_t cp);